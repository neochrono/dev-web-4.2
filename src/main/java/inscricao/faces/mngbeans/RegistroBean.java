package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import utfpr.faces.support.PageBean;

@ManagedBean
@ApplicationScoped
public class RegistroBean extends PageBean {
	private List<Candidato> candidatos = new ArrayList<>();
	
	public void adicionarCandidato(Candidato candidato) {
		this.candidatos.add(candidato);
	}
	
	public List<Candidato> getCandidatos() {
		return this.candidatos;
	}
}
